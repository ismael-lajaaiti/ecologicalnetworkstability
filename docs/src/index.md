```@meta
CurrentModule = EcologicalNetworkStability
```

# EcologicalNetworkStability

!!! warning "Work in progress"
    
    🚧🚧🚧

This package provides tools to measure the stability of ecological dynamic systems
with various metrics using Julia.

## References

  - [Dominguez-Garcia et al., 2019, PLOS](https://doi.org/10.1073/pnas.190447011)

## How can I contribute?

The easiest way to contribute is to
[open an issue](https://gitlab.com/ismael-lajaaiti/ecologicalnetworkstability/-/issues)
if you spot a bug, a typo or can't manage to do something.
Another way is to fork the repository,
start working from the `develop` branch,
and when ready, submit a pull request.

## Citing

Please mention EcologicalNetworkStability.jl
if you use it in research, teaching, or other activities.
