using Documenter
using EcologicalNetworkStability
using EcologicalNetworkStability.BEFWM2

DocMeta.setdocmeta!(
    EcologicalNetworkStability,
    :DocTestSetup,
    :(using EcologicalNetworkStability);
    recursive = true,
)

makedocs(;
    modules = [EcologicalNetworkStability],
    authors = "Ismaël Lajaaiti" * ", Iago Bonnici" * ", Sonia Kéfi",
    repo = "https://github.com/ilajaait/EcologicalNetworkStability/blob/{commit}{path}#{line}",
    sitename = "EcologicalNetworkStability.jl",
    format = Documenter.HTML(;
        prettyurls = get(ENV, "CI", "false") == "true",
        assets = String[],
    ),
    pages = [
        "Home" => "index.md",
        "EcologicalNetworkStability.jl" => "ecological-network-stability.md",
    ],
)

deploydocs(;
    repo = "gitlab.com/ismael-lajaaiti/ecologicalnetworkstability.git",
    devbranch = "doc",
)
