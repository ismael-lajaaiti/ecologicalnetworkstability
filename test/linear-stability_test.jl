@testset "Resilience" begin
    jacobian_zeros = zeros(3, 3)
    @test resilience(jacobian_zeros) == 0
end
