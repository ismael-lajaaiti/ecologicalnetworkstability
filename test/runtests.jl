# Import packages
using Documenter
using LinearAlgebra
using JuliaFormatter
using Random
using EcologicalNetworkStability
using EcologicalNetworkStability.BEFWM2
using StatsBase
using Test

# Set and print seed
seed = sample(1:100000)
Random.seed!(seed)
@info "Seed set to $seed."

# Run doctests first
DocMeta.setdocmeta!(
    EcologicalNetworkStability,
    :DocTestSetup,
    :(using EcologicalNetworkStability);
    recursive = true,
)
doctest(EcologicalNetworkStability)
println("------------------------------------------")

# Group test files
test_files = ["linear-stability_test.jl", "bioenergetic-jacobian_test.jl"]

# Set up text formatting
highlight = "\033[7m"
bold = "\033[1m"
green = "\033[32m"
reset = "\033[0m"

# Loop on test files
no_break = true
for test in test_files
    println("$(highlight)$(test)$(reset)")
    global no_break = false
    include(test) # if a test fails, the loop is broken
    global no_break = true
    println("$(bold)$(green)PASSED$(reset)")
    println("------------------------------------------")
end

# Test code formatting
if no_break
    @info "Checking source code formatting.."
    for (folder, _, files) in walkdir("..")
        for file in files
            if !any(endswith(file, ext) for ext in [".jl", ".md", ".jmd", ".qmd"])
                continue
            end
            path = joinpath(folder, file)
            println(path)
            if !format(path; overwrite = false, format_markdown = true)
                @warn "Source code in $path is not formatted according \
                to the project style defined in ../.JuliaFormatter.toml. \
                Consider formatting it using your editor's autoformatter or with \
                `using JuliaFormatter;` + \
                `format(\"path/to/EcologicalNetworkStability\", format_markdown=true)` \
                run from your usual sandbox/developing environment."
            end
        end
    end
end
