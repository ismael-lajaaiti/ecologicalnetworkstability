@testset "Resource-consumer" begin
    foodweb = FoodWeb([0 0; 1 0])
    p = ModelParameters(foodweb; functional_response = ClassicResponse(foodweb))
    B = rand(2)
    J = bioenergetic_jacobian(p, B)

    # Unpack parameters
    h = p.functional_response.h # hill exponent
    hₜ = p.functional_response.hₜ # handling times
    c = p.functional_response.c # intraspecific competition strength
    aᵣ = p.functional_response.aᵣ # attack rates
    ω = p.functional_response.ω # preferency
    e = p.biorates.e # assimilation efficiencies
    x = p.biorates.x # metabolic losses
    d = p.biorates.d # death rates
    r = p.biorates.r # intrinsic growth rates
    K = p.environment.K # carrying capacities
    m = p.network.M # species body-masses

    # J₁₁ 
    J₁₁_growth = r[1] * (1 - 2 * B[1] / K[1])
    J₁₁_death = -d[1]
    J₁₁_being_eaten1 = (h * B[1]^(h - 1)) / (1 + aᵣ[2, 1] * hₜ[2, 1] * B[1]^h)
    J₁₁_being_eaten2 =
        (aᵣ[2, 1] * hₜ[2, 1] * h * B[1]^(2 * h - 1)) / (1 + aᵣ[2, 1] * hₜ[2, 1] * B[1]^h)^2
    J₁₁_being_eaten = -B[2] * aᵣ[2, 1] * (J₁₁_being_eaten1 - J₁₁_being_eaten2)
    J₁₁_expected = J₁₁_growth + J₁₁_being_eaten + J₁₁_death
    @test J[1, 1] ≈ J₁₁_expected atol = 1e-4

    # J₁₂
    J₁₂_expected = -(aᵣ[2, 1] * B[1]^h) / (1 + aᵣ[2, 1] * hₜ[2, 1] * B[1]^h)
    @test J[1, 2] ≈ J₁₂_expected atol = 1e-4

    # J₂₁
    J₂₁_eating1 = h * B[1]^(h - 1) / (1 + aᵣ[2, 1] * hₜ[2, 1] * B[1]^h)
    J₂₁_eating2 =
        aᵣ[2, 1] * hₜ[2, 1] * h * B[1]^(2 * h - 1) / (1 + aᵣ[2, 1] * hₜ[2, 1] * B[1]^h)^2
    J₂₁_expected = e[2, 1] * aᵣ[2, 1] * B[2] * (J₂₁_eating1 - J₂₁_eating2)
    @test J[2, 1] ≈ J₂₁_expected atol = 1e-4

    # J₂₂
    J₂₂_eating = (e[2, 1] * aᵣ[2, 1] * B[1]^h) / (1 + aᵣ[2, 1] * hₜ[2, 1] * B[1]^h)
    J₂₂_loss = x[2] + d[2]
    J₂₂_expected = J₂₂_eating - J₂₂_loss
    @test J[2, 2] ≈ J₂₂_expected atol = 1e-4
end

@testset "Resource-consumer + intra. interference" begin
    foodweb = FoodWeb([0 0; 1 0])
    p = ModelParameters(foodweb; functional_response = ClassicResponse(foodweb; c = 0.2))
    B = rand(2)
    J = bioenergetic_jacobian(p, B)

    # Unpack parameters
    h = p.functional_response.h # hill exponent
    hₜ = p.functional_response.hₜ # handling times
    c = p.functional_response.c # intraspecific competition strength
    aᵣ = p.functional_response.aᵣ # attack rates
    ω = p.functional_response.ω # preferency
    e = p.biorates.e # assimilation efficiencies
    x = p.biorates.x # metabolic losses
    d = p.biorates.d # death rates
    r = p.biorates.r # intrinsic growth rates
    K = p.environment.K # carrying capacities
    m = p.network.M # species body-masses

    F₂₁_denom = (1 + c[2] * B[2] + aᵣ[2, 1] * hₜ[2, 1] * B[1]^h)

    # J₁₁ 
    J₁₁_growth = r[1] * (1 - 2 * B[1] / K[1])
    J₁₁_death = -d[1]
    J₁₁_being_eaten1 = (h * B[1]^(h - 1)) / F₂₁_denom
    J₁₁_being_eaten2 = (aᵣ[2, 1] * hₜ[2, 1] * h * B[1]^(2 * h - 1)) / F₂₁_denom^2
    J₁₁_being_eaten = -B[2] * aᵣ[2, 1] * (J₁₁_being_eaten1 - J₁₁_being_eaten2)
    J₁₁_expected = J₁₁_growth + J₁₁_being_eaten + J₁₁_death
    @test J[1, 1] ≈ J₁₁_expected atol = 1e-4

    # J₁₂
    J₁₂_being_eaten = -(aᵣ[2, 1] * B[1]^h) / F₂₁_denom
    J₁₂_interference = (c[2] * aᵣ[2, 1] * B[2] * B[1]^h) / F₂₁_denom^2
    J₁₂_expected = J₁₂_being_eaten + J₁₂_interference
    @test J[1, 2] ≈ J₁₂_expected atol = 1e-4

    # J₂₁
    J₂₁_eating1 = h * B[1]^(h - 1) / F₂₁_denom
    J₂₁_eating2 = aᵣ[2, 1] * hₜ[2, 1] * h * B[1]^(2 * h - 1) / F₂₁_denom^2
    J₂₁_expected = e[2, 1] * aᵣ[2, 1] * B[2] * (J₂₁_eating1 - J₂₁_eating2)
    @test J[2, 1] ≈ J₂₁_expected atol = 1e-4

    # J₂₂
    J₂₂_eating = (e[2, 1] * aᵣ[2, 1] * B[1]^h) / F₂₁_denom
    J₂₂_loss = x[2] + d[2]
    J₂₂_interference = (e[2, 1] * aᵣ[2, 1] * c[2] * B[2] * B[1]^h) / F₂₁_denom^2
    J₂₂_expected = J₂₂_eating - J₂₂_loss - J₂₂_interference
    @test J[2, 2] ≈ J₂₂_expected atol = 1e-4
end

@testset "Resource-consumer + intra. interference + carnivory" begin
    @test true # TODO
end
