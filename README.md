# EcologicalNetworkStability.jl

Measure the stability of dynamical systems with various metrics using Julia.
Companion package of `EcologicalNetworkDynamics.jl`.

## To-do list (:warning: Work In Progress :warning:)

  - [x] reactivity (and median)
  - [x] resilience (and median)
  - [x] maximum amplification (and median)
  - [x] time to maximum amplification (and median)
  - [x] intrinsic stochasticity invariability
  - [x] sensitivity
  - [x] tolerance to increase mortality
  - [x] tolerance to extinctions (robustness)
  - [x] resistance of total biomass to increase in mortality
  - [x] sensitivity of species biomass to increase in mortality
  - [x] resistance of community composition to random extinction
  - [x] resistance of total biomass to random deletion
  - [x] sensitivity to species biomass to random species deletion
  - [ ] jacobian from `ModelParameters` (trophic)
  - [ ] jacobian from `ModelParameters` (trophic & non-trophic)

## References

  - [Dominguez-Garcia et al. 2019](https://www.pnas.org/doi/suppl/10.1073/pnas.1904470116)
