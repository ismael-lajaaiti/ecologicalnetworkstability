# ------------------------------------------------------
# Jacobian of the bio-energetic model using Symbolics.jl
# ------------------------------------------------------

"""
Return the value of the jacobian for the given model parameters (`p`)
and evaluated in `Beq`.
"""
function get_jacobian(p::ModelParameters, Beq)
    j_expr, B = get_jacobian_expression(p)
    evaluate_jacobian_expression(j_expr, B, Beq)
end

"""
Internal function that translates the `BEFWM2.ModelParameters` into a an ODE system
understandable by Symbolics.jl.
"""
function translate_ODE_system(p::ModelParameters)
    S = richness(p.network)
    @variables B[1:S]
    dBdt = Array{Function}(undef, S)
    for i in 1:S
        G_net(i, B, p) = growth(i, B, p) + eating(i, B, p) - metabolic_loss(i, B, p)
        Δc = competition_for_space_delta(i, B, p)
        dBdt_fun(B) = (1 - Δc) * G_net(i, B, p) - death(i, B, p) - being_eaten(i, B, p)
        dBdt[i] = dBdt_fun
    end
    (dBdt, B)
end

"""
Compute the expression of the Jacobian for the given parameters.
"""
function get_jacobian_expression(p::ModelParameters)
    dBdt, B = translate_ODE_system(p)
    get_jacobian_expression(dBdt, B)
end
get_jacobian_expression(dBdt, B) = (Symbolics.jacobian([f(B) for f in dBdt], B), B)

"""
Evaluate the jacobian expression for given biomass values `Beq`.
"""
function evaluate_jacobian_expression(jacobian_expr, B, Beq)
    @assert length(Beq) == size(jacobian_expr, 1) == size(jacobian_expr, 2)
    S = length(Beq)
    substit_dict = Dict(B[i] => Beq[i] for i in 1:S)
    substitute.(jacobian_expr, (substit_dict,))
end

function being_eaten(i, B::AbstractArray{Num}, p::ModelParameters)
    A = BEFWM2.get_trophic_adjacency(p.network)
    predator_i = A[:, i].nzind
    being_eaten = 0.0
    for predator in predator_i
        being_eaten += B[predator] * F(predator, i, B, p)
    end
    being_eaten
end

function eating(i, B::AbstractArray{Num}, p::ModelParameters)
    A = BEFWM2.get_trophic_adjacency(p.network)
    prey_i = A[i, :].nzind
    e = p.biorates.e
    eating = 0
    for prey in prey_i
        eating += e[i, prey] * B[i] * F(i, prey, B, p)
    end
    eating
end

"""
Classic functional response.
"""
function F(i, j, B::AbstractArray{Num}, p::ModelParameters)
    # Unpack parameters
    ω = p.functional_response.ω
    a_i = p.functional_response.aᵣ[i, :]
    a_i = a_refuge(a_i, i, B, p) # eventual refuge effect on the attack rate
    c = p.functional_response.c
    h = p.functional_response.h
    hₜ = p.functional_response.hₜ
    m_i = p.network.M[i]

    # Compute functional response
    prey_i = ω[i, :].nzind
    sum_prey = 0
    for prey in prey_i
        sum_prey += ω[i, prey] * a_i[prey] * hₜ[i, prey] * B[prey]^h
    end
    denom = 1 + c[i] * B[i] + interference_term(i, B, p) + sum_prey
    denom *= m_i
    num = ω[i, j] * a_i[j] * B[j]^h
    num / denom
end

function competition_for_space_delta(i, B::AbstractArray{Num}, p::ModelParameters)
    isa(p.network, MultiplexNetwork) || return 0.0
    c_layer = p.network.layers[:c]
    competing_sp = c_layer.A[:, i].nzind
    c0 = c_layer.intensity
    c_term = 0
    for c_sp in competing_sp
        c_term += B[c_sp]
    end
    θ(c0 * c_term)
end

"""
Interspecific interference term in the denominator of the functional response.
"""
function interference_term(i, B::AbstractArray{Num}, p::ModelParameters)
    isa(p.network, MultiplexNetwork) || return 0.0
    i_layer = p.network.layers[:i]
    interfering_sp = i_layer.A[:, i].nzind
    i0 = i_layer.intensity
    i_term = 0
    for i_sp in interfering_sp
        i_term += B[i_sp]
    end
    i0 * i_term
end

"""
Producer growth.
"""
function growth(i, B::AbstractArray{Num}, p::ModelParameters)
    BEFWM2.isproducer(i, p.network) || return 0.0
    r_i = p.biorates.r[i]
    r_i_fac = r_i * (1 + facilitation_delta(i, B, p))
    α = p.producer_competition.α
    K_i = p.environment.K[i]
    s = 0
    for j in 1:length(B)
        s += α[i, j] * B[i]
    end
    r_i_fac * B[i] * (1 - s / K_i)
end

"""
Attack rate taking into account the effect of refuge provisioning.
"""
function a_refuge(a_i, i, B::AbstractArray{Num}, p::ModelParameters)
    isa(p.network, MultiplexNetwork) || return a_i
    r_layer = p.network.layers[:r]
    providing_refuge_sp = r_layer.A[:, i].nzind
    Δr = 0
    for r_sp in providing_refuge_sp
        Δr += B[r_sp]
    end
    Δr *= r_layer.intensity
    a_i = a_i ./ (1 + Δr)
end

"""
The effect of plant facilitation is translated as: r ⟹ r(1 + Δr). Return Δr.
"""
function facilitation_delta(i, B::AbstractArray{Num}, p::ModelParameters)
    isa(p.network, MultiplexNetwork) || return 0.0
    f_layer = p.network.layers[:f]
    facilitating_sp = f_layer.A[:, i].nzind
    s = 0
    for f_sp in facilitating_sp
        s += B[f_sp]
    end
    f_layer.intensity * s # f0 ΣₖBₖ
end

"""
Expression of the natural death rate.
"""
death(i, B::AbstractArray{Num}, p::ModelParameters) = p.biorates.d[i] * B[i]

"""
Expression of the metabolic loss.
"""
metabolic_loss(i, B::AbstractArray{Num}, p::ModelParameters) = p.biorates.x[i] * B[i]

"""
Heavise function: return 1 if x > 0 and 0 otherwise.
"""
θ(x) = abs(sign(x)) * (1 + sign(x)) / 2
