# ----------------------------------------------
# Analytical jacobian for the bioenergetic model
# ----------------------------------------------

"""
    bioenergetic_jacobian(parameters::ModelParameters, B)

Compute analytically the jacobian of the bio-energetic model given:

  - `parameters` the system parameters given under the `ModelParameters` type
    from the [EcologicalNetworkDynamics](https://docs-url) package

  - `B` the vector of species biomasses, usually biomasses at an equilibrium point

# Examples

```jldoctest
julia> # to fill

```

# References

  - [Documentation of EcologicalNetworkDynamics package](https://docs-url)

See also [`resilience`](@ref) and [`reactivity`](@ref).
"""
function bioenergetic_jacobian(parameters::ModelParameters, B)
    # Unpack parameters
    h = parameters.functional_response.h # hill exponent
    hₜ = parameters.functional_response.hₜ # handling times
    c = parameters.functional_response.c # intraspecific competition strength
    aᵣ = parameters.functional_response.aᵣ # attack rates
    ω = parameters.functional_response.ω # preferency
    e = parameters.biorates.e # assimilation efficiencies
    x = parameters.biorates.x # metabolic losses
    d = parameters.biorates.d # death rates
    r = parameters.biorates.r # intrinsic growth rates
    K = parameters.environment.K # carrying capacities
    m = parameters.network.M # species body-masses
    network = parameters.network

    S = richness(network)
    predators = [predators_of(i, network) for i in 1:S]
    preys = [preys_of(i, network) for i in 1:S]
    prods = producers(network)
    F = parameters.functional_response(B, network)
    J = zeros(Float64, S, S) # initialize Jacobian matrix
    for i in 1:S, j in 1:S
        if i == j # diagonal terms 
            growth = Jii_growth(i, prods, B, r, K)
            eating = Jii_eating(i, B, e, ω, aᵣ, hₜ, h, c, m, F)
            being_eaten = Jii_being_eaten(i, predators, B, ω, aᵣ, hₜ, h, c, m, F)
            J[i, j] = growth + eating + being_eaten - x[i] - d[i]
        else # off-diagonal terms 
            eating = Jij_eating(i, j, preys, B, e, ω, aᵣ, hₜ, h, c, m)
            being_eaten = Jij_being_eaten(i, j, predators, B, ω, aᵣ, hₜ, h, c, m)
            J[i, j] = eating + being_eaten
        end
    end
    J
end

# Functions to compute the jacobian off-diagonal terms
# ----------------------------------------------------
"""
Compute the contribution due to eating for the off-diagonal jacobian terms.
"""
function Jij_eating(i, j, preys, B, e, ω, aᵣ, hₜ, h, c, m)
    j ∈ preys[i] || return 0
    ∑preyᵢ = ∑prey(i, ω, aᵣ, hₜ, B, h)
    direct_term = e[i, j] * ω[i, j] * aᵣ[i, j] * h * B[j]^(h - 1)
    direct_term /= m[i] * (1 + c[i] * B[i] + ∑preyᵢ)
    indirect_term = sum(e[i, :] .* ω[i, :] .* aᵣ[i, :] .* B .^ h)
    indirect_term *= ω[i, j] * aᵣ[i, j] * hₜ[i, j] * h * B[j]^(h - 1)
    indirect_term /= m[i] * (1 + c[i] * B[i] + ∑preyᵢ)^2
    B[i] * (direct_term - indirect_term)
end

"""
Compute the contribution due to being eaten for the off-diagonal jacobian terms.
"""
function Jij_being_eaten(i, j, predators, B, ω, aᵣ, hₜ, h, c, m)
    Fji = ω[j, i] * aᵣ[j, i] * B[i]^h
    Fji /= m[j] * (1 + c[j] * B[j] + ∑prey(j, ω, aᵣ, hₜ, B, h))
    shared_predators = 0
    for k in predators[i]
        if k ∈ predators[j] ∪ [j]
            term = ω[k, j] * aᵣ[k, j] * hₜ[k, j] * h * B[j]^(h - 1)
            j == k && (term += c[j])
            term *= B[k] * ω[k, i] * aᵣ[k, i]
            term /= m[k] * (1 + c[k] * B[k] + ∑prey(k, ω, aᵣ, hₜ, B, h))^2
            shared_predators += term
        end
    end
    shared_predators *= B[i]^h
    -(Fji - shared_predators)
end

# Functions to compute the jacobian diagonal terms
# ------------------------------------------------
"""
Compute the contribution due to growth for the diagonal jacobian terms.
"""
function Jii_growth(i, prods, B, r, K)
    i ∈ prods || return 0
    r[i] * (1 - 2 * B[i] / K[i])
end

"""
Compute the contribution due to eating for the diagonal jacobian terms.
"""
function Jii_eating(i, B, e, ω, aᵣ, hₜ, h, c, m, F)
    ∑eᵢₖFᵢₖ = sum(e[i, :] .* F[i, :])
    ∑preyᵢ = ∑prey(i, ω, aᵣ, hₜ, B, h)
    ii_term = h * B[i]^h * e[i, i] * ω[i, i] * aᵣ[i, i]
    ii_term /= m[i] * (1 + c[i] * B[i] + ∑preyᵢ)^2
    prey_term = sum(e[i, :] .* ω[i, :] .* aᵣ[i, :] .* B .^ h)
    prey_term *= B[i] * (ω[i, i] * aᵣ[i, i] * hₜ[i, i] * h * B[i]^(h - 1) + c[i])
    prey_term /= m[i] * (1 + c[i] * B[i] + ∑preyᵢ)^2
    ∑eᵢₖFᵢₖ - prey_term + ii_term
end

"""
Compute the contribution due to being eaten for the diagonal jacobian terms.
"""
function Jii_being_eaten(i, predators, B, ω, aᵣ, hₜ, h, c, m, F)
    Fᵢᵢ = F[i, i]
    sum1, sum2 = 0, 0
    for k in predators[i]
        # Contribution to sum1
        ∑preyₖ = ∑prey(k, ω, aᵣ, hₜ, B, h)
        term1 = B[k] * ω[k, i] * aᵣ[k, i]
        term1 /= m[k] * (1 + c[k] * B[k] + ∑preyₖ)
        sum1 += term1
        # Contribution to sum2
        term2 = i == k ? c[i] : 0
        term2 += ω[k, i] * aᵣ[k, i] * hₜ[k, i] * h * B[i]^(h - 1)
        term2 *= ω[k, i] * aᵣ[k, i] * B[k]
        term2 /= m[k] * (1 + c[k] * B[k] + ∑preyₖ)^2
        sum2 += term2
    end
    sum1 *= h * B[i]^(h - 1)
    sum2 *= B[i]^h
    -(Fᵢᵢ + sum1 - sum2)
end

∑prey(i, ω, aᵣ, hₜ, B, h) = sum(ω[i, :] .* aᵣ[i, :] .* hₜ[i, :] .* B .^ h)
