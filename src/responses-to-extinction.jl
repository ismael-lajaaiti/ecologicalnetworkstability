# ------------------------------
# Responses to press extinctions
# ------------------------------

"""
    resistance_to_extinctions(solution)

Measure the resistance of the community to species extinction with the 3 following metrics:

  - ``\\text{CE}`` **resistance of community composition to species extinctions**
    quantified by the number of cascading extinctions after removing one species.

  - ``\\text{RE}``: **resistance of total biomass to species extinctions**
    quantified by the number of absolute relative change in total biomass.
    We express more formally this metric below,
    where ``B^\\text{init}_\\text{tot}`` and ``B^\\text{init}_\\text{tot}``
    are respectively the total biomass before and after the species extinction.

```math
\\text{RE} = - \\frac{|B^\\text{end}_\\text{tot} - B^\\text{init}_\\text{tot}|}
{B^\\text{init}_\\text{tot}}
```

  - ``\\text{SE}``: **sensitivity of species biomass to species extinctions**
    quantified by the sum of absolute changes in each species biomass.
    We express more formally this metric below,
    where ``B_i`` denotes the biomass of species i.

```math
\\text{SE} = - \\frac{ \\sum_{i=1}^S |B^\\text{end}_i - B^\\text{init}_i|}
            {B^\\text{init}_\\text{tot}}
```

The definition aboves give a value for a single species extinction.
Thus, for each of the 3 metrics we record two quantity:
the average over all species extinctions (performed one by one)
and the absolute maximum value (i.e. the worst case).

Formally, let's call ``M_i`` the metric of interest after the extinction of species i
where ``M`` is either ``\\text{CE}``, ``\\text{RE}`` or ``\\text{SE}``.
Then we record:

 1. ``\\text{M}_\\text{mean} = \\frac{1}{S} \\sum_{i=1}^S \\text{M}_i``

 2. ``\\text{M}_\\text{max} = \\text{sign}(M) \\cdot \\text{max}_{i=1}^S |\\text{M}_i|``

# Output

Named tuple containing containing the mean and the max of
``\\text{CE}``, ``\\text{RE}`` and ``\\text{SE}``.

# Examples

## Independent producers

We begin with a simple example of independent species all producers.
At equilibrium, all their biomasses are 1.
Moreover, as they do not interact with each other,
extinguishing a species has no effect on the others.
Thus we expect all the metrics to be zero.

```jldoctest
julia> using EcologicalNetworkStability.BEFWM2

julia> only_producers_web = FoodWeb([0 0 0; 0 0 0; 0 0 0]);

julia> params = ModelParameters(only_producers_web);

julia> sol = simulate(params, [0.5]; verbose = false);

julia> res_to_ext = resistance_to_extinctions(sol);

julia> all(abs.(collect(res_to_ext)) .< 1e-4)
true
```

## Cascading extinctions in a food-chain

We consider a chain where species 1 is a producer and is eaten by species 2
which is itself eaten by species 3.

For the food-chain we expect the following regarding cascading extinctions,
if we extinguish:

  - the top-predator (species 3), the 2 remaining species can survive

  - the herbivor (species 2), the top-predator goes extinct  but the producer survives
  - the producer (species 1), the 2 remaining species goes extinct in cascade

In short, we expect to have a maximum cascading extinction of 2
and a mean cascading extinction of 1.

```jldoctest
julia> using EcologicalNetworkStability.BEFWM2

julia> foodchain = FoodWeb([0 0 0; 1 0 0; 0 1 0]);

julia> params = ModelParameters(foodchain; biorates = BioRates(foodchain; d = 0));

julia> sol = simulate(params, [0.5]; verbose = false);

julia> isempty(get_extinct_species(sol)) # all species survived
true

julia> res_to_ext = resistance_to_extinctions(sol);

julia> res_to_ext.CE_max == 2.0 # max cascading extinction is 2 (when prod. extinct)
true

julia> res_to_ext.CE_mean == 1.0 # mean cascading extinction is 1 (0+1+2)/3
true
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Thébault et al. 2006 - Cascading extinctions and ecosystem functioning]
    (https://doi.org/10.1111/j.2006.0030-1299.15007.x)

See also [`biomass_resistance`](@ref), [`biomass_sensitivity`](@ref),
[`cascading_extinctions`](@ref).
"""
function resistance_to_extinctions(solution)
    params = get_parameters(solution)
    net = params.network
    S = richness(net)
    extinct_sp = keys(get_extinct_species(solution))
    alive_sp = filter(x -> x ∉ extinct_sp, 1:S)
    n_extinct, n_alive = length(extinct_sp), length(alive_sp)
    Beq = solution[end] # biomass at equilibrium
    metric_fun = [cascading_extinctions, biomass_resistance, biomass_sensitivity]
    metric_mat = zeros(Float64, n_alive, length(metric_fun))
    for (i, sp_to_extinct) in enumerate(alive_sp)
        B0 = copy(Beq)
        B0[sp_to_extinct] = 0 # extinct a species
        sol_temp = simulate(params, B0; verbose = false)
        Bend = sol_temp[end]
        args = [
            [n_extinct + 1, length(keys(get_extinct_species(sol_temp)))],
            [B0, Bend],
            [B0, Bend],
        ]
        for (j, fun) in enumerate(metric_fun)
            metric_mat[i, j] = fun(args[j]...)
        end
    end
    (
        CE_mean = mean(metric_mat[:, 1]),
        CE_max = maximum(metric_mat[:, 1]),
        RE_mean = mean(metric_mat[:, 2]),
        RE_max = minimum(metric_mat[:, 2]),
        SE_mean = mean(metric_mat[:, 3]),
        SE_max = minimum(metric_mat[:, 3]),
    )
end

"""
    biomass_resistance(B_init, B_end)

Resistance of total biomass after a perturbation
quantified as the absolute relative difference in the total biomass
cause by the perturbation.

# Arguments

  - `B_init` vector of species biomasses before the perturbation

  - `B_end` vector of species biomasses after the perturbation

# Example

```jldoctest
julia> biomass_resistance([1, 1], [1, 2])
-0.5

julia> biomass_resistance([1, 1], [1, 0])
-0.5

julia> biomass_resistance([1, 1], [0, 2])
0.0
```

See also [`biomass_sensitivity`](@ref).
"""
biomass_resistance(B_init, B_end) = -abs(sum(B_end) - sum(B_init)) / sum(B_init)

"""
    biomass_sensitivity(B_init, B_end)

Sensitivity of species biomass after a perturbation
quantified as the sum of the absolute differences in each species biomass
relative to the initial total biomass.

# Arguments

  - `B_init` vector of species biomasses before the perturbation

  - `B_end` vector of species biomasses after the perturbation

# Example

```jldoctest
julia> biomass_sensitivity([1, 1], [1, 2])
-0.5

julia> biomass_sensitivity([1, 1], [1, 0])
-0.5

julia> biomass_sensitivity([1, 1], [0, 2])
-1.0
```

See also [`biomass_resistance`](@ref).
"""
biomass_sensitivity(B_init, B_end) = -sum(abs.(B_end - B_init)) / sum(B_init)


"""
    cascading_extinctions(extinct_sp_init, extinct_sp_end)

Number of cascading extinctions following a perturbation,
defined as the difference of
the number of species extinct before the perturbation (`extinct_sp_init`) and
the number of species extinct after the perturbation (`extinct_sp_end`).

# Example

```jldoctest
julia> cascading_extinctions(1, 2)
1

julia> cascading_extinctions(1, 10)
9
```
"""
function cascading_extinctions(extinct_sp_init, extinct_sp_end)
    @assert extinct_sp_init >= 0
    @assert extinct_sp_end >= 0
    @assert extinct_sp_end >= extinct_sp_init
    extinct_sp_end - extinct_sp_init
end

"""
    robustness(solution; n_rep = 100)

Measure the robustness of a community defined as one minus the extinction area,
where the extinction area correspond to the area under the curve
of the fraction of secundary extinctions versus the fraction of primary extinctions.

The primary extinctions are performed in a random order.
We compute the average robustness for `n_rep` different random orders.

# Examples

## Independent producers

We consider a system of two independent producers.
In this system, there is no cascading extinctions
then the fraction secundary extinctions is equal to
the fraction of primary extinctions.
Then the robustness is ``r = 0.5``.

```jldoctest
julia> using EcologicalNetworkStability.BEFWM2;

julia> two_producers = FoodWeb([0 0; 0 0]);

julia> params = ModelParameters(two_producers);

julia> sol = simulate(params, [1]);

julia> robustness(sol) == 0.5
true
```

## Food-chain

We now consider a 2-species food-chain.
Depending on the order of the extinction events,
we expect different robustness values.
If the predator is extinguished first,
then the producer can survive and the fraction of primary and secundary extinctions
are equal to `[0.0, 0.5, 1.0]`.
This scenario gives a robustness of ``r = 0.5``.

If the producer is extinguished first then predator goes extinct in cascade,
resulting in the following fraction of secundary extinctions: `[0.0, 1.0, 1.0]`
which corresponds to a robustness of ``r = 0.25``.

In short, on average we expect a robustness of ``r = \\frac{0.5 + 0.25}{2} = 0.375``.

```jldoctest
julia> using EcologicalNetworkStability.BEFWM2;

julia> chain = FoodWeb([0 0; 1 0]);

julia> params = ModelParameters(chain);

julia> sol = simulate(params, [0.5]);

julia> isapprox(robustness(sol; n_rep = 1_000), 0.375; atol = 1e-2)
true
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Dunne et al. 2002 - Robustness increases with connectance]
    (https://doi.org/10.1046/j.1461-0248.2002.00354.x)

See also [`resistance_to_extinctions`](@ref).
"""
function robustness(solution; n_rep = 100)
    params = get_parameters(solution)
    S = richness(params.network)
    extinct_sp_init = keys(get_extinct_species(solution))
    alive_sp_init = filter(x -> x ∉ extinct_sp_init, 1:S)
    n_alive_init = length(alive_sp_init)
    robustness_vec = zeros(Float64, n_rep)
    primary_ext = [i / n_alive_init for i in 0:n_alive_init]
    for k in 1:n_rep
        Beq = copy(solution[end])
        extinct_sp = copy(extinct_sp_init)
        alive_sp = copy(alive_sp_init)
        secundary_ext = zeros(Float64, n_alive_init + 1)
        count = 0
        while length(extinct_sp) < S
            count += 1
            sp_to_extinguish = rand(alive_sp)
            Beq[sp_to_extinguish] = 0.0
            sol = simulate(params, Beq; verbose = false)
            extinct_sp = keys(get_extinct_species(sol))
            alive_sp = filter(x -> x ∉ extinct_sp, 1:S)
            secundary_ext[count+1] = length(extinct_sp) / n_alive_init
        end
        count + 2 > (S + 1) || (secundary_ext[(count+2):end] .= 1.0)
        robustness_vec[k] = 1 - ∫(primary_ext, secundary_ext)
    end
    mean(robustness_vec)
end

"""
    ∫(x, y)

Compute the numerical integral of the coordinates (`x`, `y`),
using the mid-point integration method.

# Examples

Constant values:

```jldoctest
julia> EcologicalNetworkStability.∫([0, 5, 10], [1, 1, 1]) == 10.0
true
```

Dummy non-constant values:

```jldoctest
julia> EcologicalNetworkStability.∫([0, 1, 2], [0, 0, 10]) == 5.0
true
```

Integral of sinus between 0 and π (expected to be 2):

```jldoctest
julia> x = LinRange(0, π, 1_000);

julia> y = sin.(x);

julia> isapprox(EcologicalNetworkStability.∫(x, y), 2.0; atol = 1e-5)
true
```
"""
function ∫(x, y)
    @assert length(x) == length(y)
    area = 0
    n = length(y)
    for i in 1:n-1
        y_mid = mean(y[[i, i + 1]])
        Δx = x[i+1] - x[i]
        area += y_mid * Δx
    end
    area
end
