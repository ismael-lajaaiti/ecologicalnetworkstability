# -------------------------------
# Responses to press perturbation
# -------------------------------

# Sensitivity
# -----------
"""
    sensitivity(jacobian::AbstractArray)

Return the sensitivity to a press perturbation of a community
at the community and species scales `(s_com, s_sp)`.

The sensitivity matrix is defined as the inverse of the Jacobian: ``S = - J^{-1}``.
``S_{ij}`` gives the sensitivity of species ``i``
if a press perturbation is applied to species ``j``.
Thus, we define two stability metrics from ``S``:

  - `s_com` sensitivity at the community level:
    the average of the absolute changes in species biomasses

  - `s_sp` sensitivity at the species level:
    the average strength of the coefficients of ``S``

```math
\\begin{aligned}
s_\\text{com} &=  \\frac{1}{N} \\sum_i | \\sum_j S_{ij} | \\\\
s_\\text{sp} &= \\frac{1}{N^2} \\sum_i \\sum_j | S_{ij} |
\\end{aligned}
```

# Examples

```jldoctest
julia> jacobian = [-1 0; 0 -2];

julia> sensitivity(jacobian)
(s_com = 0.75, s_sp = 0.375)
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Carpenter et al. 1992 - Resilience and resistance of a lake phosphorus cycle]
    (https://doi.org/10.1086/285440)
"""
function sensitivity(jacobian::AbstractArray)
    S = -inv(jacobian)
    s_com = mean(abs.(sum(S; dims = 2)))
    s_sp = mean(abs.(S))
    (s_com = s_com, s_sp = s_sp)
end

"""
    resistance_to_mortality(solution)

Measure the resistance and the sensitivity of the community to increased mortality
with 3 metrics of resistance and 3 metrics of sensisitivity.

For the resistance we define:

  - ``\\text{RM}^\\text{g}``: the global resistance to mortality
    for which all species death rate are simultaneously increased.

  - ``\\text{RM}^\\text{l}_\\text{mean}``: the mean local resistance to mortality
    for which species death rate is increased species per species (separately)
    and then we record the average resistance.
  - ``\\text{RM}^\\text{l}_\\text{max}``: the minimal local resistance to mortality
    for which species death rate is increased species per species (separately)
    and then we record the absolute maximum value (i.e. the worst case).

We define the resistance of community as the absolute relative difference
in the community total biomass due to a perturbation.
Formally:

```math
\\text{R} = - \\frac{|B_\\text{tot}^\\text{end} - B_\\text{tot}^\\text{init}|}
    {B_\\text{tot}^\\text{init}}
```

where ``B^\\text{init}_\\text{tot}`` and ``B^\\text{init}_\\text{tot}``
are respectively the total biomass before and after the species extinction.

Moreover, for the sensitivity we define ``\\text{SM}^\\text{g}``,
``\\text{SM}^\\text{l}_\\text{mean}`` and ``\\text{SM}^\\text{l}_\\text{max}``
in a similar manner as for the resistance.
However, instead of evaluating the absolute relative difference
in the community total biomass,
we evaluate the summed absolute differences in each species biomass due to the perturbation.
Formally:

```math
\\text{S} = - \\frac{\\sum_{i=1}^S |B_i^\\text{end} - B_i^\\text{init}|}
    {B_\\text{tot}^\\text{init}}
```

# Examples

## Independent producers

We consider a system with 2 independent and identical producers.
First, we expect to have
``\\text{RM}^\\text{l}_\\text{mean} = \\text{RM}^\\text{l}_\\text{max}``
because both producers have the same response to an increase in their mortality
as they are identical.
Then increasing the mortality of producer 1 or 2 has the same effect,
which implies that the mean (local) resistance is also the maximum.

Secondly, we expect that the global resistance is the double of the local resistance
because the two species are independent.
Indeed, if we increase the mortality of both producers,
they will both react as if they were perturbed individually (because they are independent)
then total resistance is the sum of the individual response which correspond to
two times the local resistance.

Thirdly, as there is no compensatory effects in this simple system,
both producers biomass decrease when mortality increase,
the resistance and the sensitivity should be equal.

```jldoctest
julia> using EcologicalNetworkStability.BEFWM2;

julia> two_producers = FoodWeb([0 0; 0 0]);

julia> params = ModelParameters(two_producers);

julia> sol = simulate(params, [0.5]; verbose = false);

julia> all(isapprox.(sol[end], 1 .- params.biorates.d; atol = 1e-4)) # expected Beq
true

julia> rm = resistance_to_mortality(sol);

julia> isapprox(rm.RM_l_mean, rm.RM_l_max; atol = 1e-4)
true

julia> isapprox(rm.RM_g, 2 * rm.RM_l_mean; atol = 1e-4)
true

julia> isapprox(rm.RM_g, rm.SM_g; atol = 1e-4)
true

julia> isapprox(rm.RM_l_mean, rm.SM_l_mean; atol = 1e-4)
true

julia> isapprox(rm.RM_l_max, rm.SM_l_max; atol = 1e-4)
true
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Ives et al. 2004 - Food-web interactions govern the resistance of communities]
    (https://doi.org/10.1038/nature02515)

See also [`resistance_to_extinctions`](@ref), [`biomass_resistance`](@ref)
and [`biomass_sensitivity`](@ref).
"""
function resistance_to_mortality(solution; δd = 0.1)
    # Set-up.
    params = deepcopy(get_parameters(solution))
    net = params.network
    S = richness(net)
    extinct_sp = keys(get_extinct_species(solution))
    alive_sp = filter(x -> x ∉ extinct_sp, 1:S)
    n_alive = length(alive_sp)
    Beq = solution[end] # biomass at equilibrium
    rm_l = zeros(Float64, n_alive) # vector to store local resistance
    sm_l = zeros(Float64, n_alive) # vector to store local sensitivity
    d = copy(params.biorates.d)

    # Local resistance (mean and max): species death rate are increased one by one.
    for (i, sp) in enumerate(alive_sp)
        d_increased_l = copy(d)
        d_increased_l[sp] *= 1 + δd
        params.biorates.d = d_increased_l
        sol = simulate(params, Beq; verbose = false)
        rm_l[i] = biomass_resistance(Beq, sol[end])
        sm_l[i] = biomass_sensitivity(Beq, sol[end])
    end
    RM_l_mean, RM_l_max = mean(rm_l), minimum(rm_l)
    SM_l_mean, SM_l_max = mean(sm_l), minimum(sm_l)

    # Global resistance: all species death rate are simulatenously increased.
    d_increased_g = zeros(Float64, S)
    for i in 1:S
        d_increased_g[i] = i ∈ alive_sp ? d[i] * (1 + δd) : d[i]
    end
    params.biorates.d = d_increased_g
    sol = simulate(params, Beq; verbose = false)
    RM_g = biomass_resistance(Beq, sol[end])
    SM_g = biomass_sensitivity(Beq, sol[end])
    (
        RM_g = RM_g,
        RM_l_mean = RM_l_mean,
        RM_l_max = RM_l_max,
        SM_g = SM_g,
        SM_l_mean = SM_l_mean,
        SM_l_max = SM_l_max,
    )
end

"""
    tolerance_to_mortality(solution; δd = 1e-2, Δd_max = 1_000)

Measure the maximum increase in death rate that the community can withstand
before any species goes extinct.

Specifically we distinguish 3 types of tolerance to mortality increase:

  - ``\\text{TM}^\\text{g}``: we increment the death rate of all species
    *simultaneously* until a species ``i`` goes extinct.
    Then we record: ``\\frac{\\Delta d_i}{d_i}`` where ``\\Delta d_i`` is
    the miminum increase in species ``i`` death rate leading to its extinction
    and ``d_i`` is the species ``i`` death rate pre-pertubation.

  - ``\\text{TM}^\\text{l}_\\text{mean}``: we increment the death rate of
    each species *separately* until a species goes extinct.
    If we write ``i \\in \\{1, 2, ..., S\\}`` the species of which we are increasing
    the death rate, we record: ``\\frac{\\Delta d_i}{d_i}`` (keeping notations from above)
    This gives us the tolerance of species ``i``,
    then we repeat this process for all species
    and we record average tolerance.
  - ``\\text{TM}^\\text{l}_\\text{min}``: same process as above,
    but instead of computing the average tolerance over the species perturbed,
    we compute the minimal tolerance (i.e. the worst case).

# Keyword arguments

To measure the tolerance of the community, we increment the mortality rate ``d``
until a species goes extinct.

  - `δd` is the increment size, i.e. we increase ``d`` by ``\\delta d`` at each step

  - `Δd_max` is maximum quantity by which the death rate can be increased.
    This is a safeguard against infinite `while` loop.
    If `Δd_max` is reached an error is returned.

# Example

## Independent producers

We consider a community of 2 independent producers.
In general, their biomass at equilibrium is given by:

```math
N^* = K(1 - \\frac{d}{r})
```

As their carrying capacity ``K`` and their intrinsic growth rate is 1,
we have ``N^* = 1 - d``.
Then we can compute easily the expected tolerance to increase in mortality.
We consider a death rate of 1 for both producers before the perturbation.
Then we know that they will go extinct when
``d`` is increased by ``\\Delta d = 0.9``,
which gives:

```math
\\text{TM} = \\frac{\\Delta d}{d} = \\frac{0.9}{0.1} = 9
```

This reasoning holds for the 3 metrics of tolerance as the producers are independent.

```jldoctest
julia> using EcologicalNetworkStability.BEFWM2;

julia> two_prod = FoodWeb([0 0; 0 0]);

julia> params = ModelParameters(two_prod; biorates = BioRates(two_prod; d = 0.1));

julia> sol = simulate(params, [1.0]);

julia> tm = tolerance_to_mortality(sol);

julia> all(collect(tm) .== 9.0)
true
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Rohr et al. 2014 - On the structural stability of mutualistic systems]
    (https://doi.org/10.1126/science.1253497)
  - [Grilli et al. 2017 - Feasibility of large ecological communities]
    (https://doi.org/10.1038/ncomms14389)
  - [Wootton et al. 2019 - Species' traits,food-web complexity and response to press]
    (https://doi.org/10.1002/ecs2.1518)

See also [`resistance_to_mortality`](@ref).
"""
function tolerance_to_mortality(solution; δd = 1e-2, Δd_max = 1_000)
    extinct_sp = keys(get_extinct_species(solution))
    S = richness(get_parameters(solution).network)
    alive_sp = filter(x -> x ∉ extinct_sp, 1:S)
    # Global tolerance: perturbation applied to all alive species simultaneously.
    TM_g = tolerance_to_mortality(solution, alive_sp, δd, Δd_max)
    # Local tolerance: perturbation applied species by species separately.
    TM_l_vec = zeros(Float64, length(alive_sp))
    for (i, sp) in enumerate(alive_sp)
        TM_l_vec[i] = tolerance_to_mortality(solution, [sp], δd, Δd_max)
    end
    TM_l_mean, TM_l_min = mean(TM_l_vec), minimum(TM_l_vec)
    (TM_g = TM_g, TM_l_mean = TM_l_mean, TM_l_min = TM_l_min)
end

function tolerance_to_mortality(solution, perturbed_sp, δd, Δd_max)
    params = deepcopy(get_parameters(solution))
    extinct_sp_init, extinct_sp_end = keys(get_extinct_species(solution)), []
    n_extinct_init, n_extinct_end = length(extinct_sp_init), length(extinct_sp_init)
    Beq = solution[end] # biomass at equilibrium
    d = params.biorates.d
    d_increased = copy(params.biorates.d) # initialize vector of new death rates
    count = 0
    while (n_extinct_end == n_extinct_init) && (count * δd < Δd_max)
        count += 1
        for sp in perturbed_sp
            d_increased[sp] += δd
        end
        params.biorates.d = d_increased
        sol = simulate(
            params,
            Beq;
            tmax = 100_000,
            verbose = false,
            callback = ExtinctionCallback(1e-5, false),
        )
        Beq = sol[end]
        extinct_sp_end = keys(get_extinct_species(sol))
        n_extinct_end = length(extinct_sp_end)
    end
    count * δd < Δd_max || @error "Maximum iteration steps reached."
    new_extinct_sp = findall(sp -> sp ∉ extinct_sp_init, collect(extinct_sp_end))
    d_i = length(perturbed_sp) == 1 ? d[perturbed_sp] : d[new_extinct_sp]
    mean((count * δd) ./ d_i)
end
