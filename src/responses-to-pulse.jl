# -------------------------------
# Responses to pulse perturbation
# -------------------------------

# Resilience
# ----------
"""
    resilience(jacobian::AbstractArray)

The resilience ``R_\\infty`` is the opposite of
the maximum real part of the `jacobian` (``J``) eigen values.

```math
R_\\infty = - \\max_i \\Re (\\lambda_i(J))
```

The system is stable if and only if ``R_\\infty > 0``.

# Examples

```jldoctest
julia> jacobian_zeros = zeros(3, 3);

julia> resilience(jacobian_zeros)
-0.0

julia> jacobian_real = [-1 0 0; 0 -2 0; 0 0 -3];

julia> resilience(jacobian_real)
1.0

julia> jacobian_complex = [-1+1im 0 0; 0 -2+1im 0; 0 0 -3+1im];

julia> resilience(jacobian_complex)
1.0
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Pimm and Lawton 1977 - Number of trophic levels in ecological communities]
    (https://www.nature.com/articles/268329a0)

See also [`reactivity`](@ref).
"""
resilience(jacobian::AbstractArray) = -maximum(real.(eigvals(jacobian)))

"""
    median_return_rate(
        jacobian::AbstractArray;
        perturbation::Symbol = :uniform,
        B_eq = nothing,
    )

Median return rate on the perturbation directions averaged over time.

```math
\\mathbb{M}(R_t^\\text{avg}) \\approx - \\frac{\\ln \\text{Tr} (e^{Jt} C e^{J^\\text{T} t})
- \\ln \\text{Tr}(C)}{2t}
```

where ``J`` is the Jacobian
and ``C`` the perturbation correlation matrix.
Specifically, if the perturbation is assumed to be equal on average for all species
(case where `perturbation = :uniform`)
then ``C = \\frac{1}{n} \\mathbb{1}``.
If the perturbation is assumed to be proportional to the equilibrium species biomasses
given by `B_eq` (case where `perturbation = :prop_to_biomass`).
then ``C = \\frac{1}{\\sum_i B_{\\text{eq},ii}^2} \\text{diag}(B_\\text{eq})^2``.

# Keyword arguments

  - `perturbation` gives the type of perturbation:
    either `:uniform` if species are assumed to be perturbed equally,
    or `:prop_to_biomass` if species are perturbed proportionnally to their biomasses at
    equilibrium. By default `perturbation = :uniform`.

  - `B_eq` species biomasses at equilibrium.
    Should only be provided if `perturbation = :prop_to_biomass`.
    By default `B_eq = nothing`.
  - `threshold` defining when the return rate has converged, formally the condition for
    convergence is ``|R_t^\\text{avg} - R_{t-1}^\\text{avg}| < \\varepsilon \\delta t``
    where ``\\varepsilon`` is the `threshold` and ``\\delta t`` is `δt`,
    by default `threshold = 1e-3`.
  - `δt` controls the size of the timestep at which the average return rate are computed,
    by default `δt = 1`.
  - `t_safe` is the maximum time after which the `while` loop is stopped for safety reasons,
    by default `t_safe = 1_000`.

# Examples

```jldoctest
julia> jacobian = [-1 0; 0 -1];

julia> median_return_rate(jacobian)
1.0

julia> jacobian = [-1 0; 0 -2];

julia> median_return_rate(jacobian) > 1
true

julia> Rₜ_uniform = median_return_rate(jacobian);

julia> B_eq, pert = [1, 1], :prop_to_biomass;

julia> Rₜ_biomass = median_return_rate(jacobian; perturbation = pert, B_eq = B_eq);

julia> Rₜ_uniform == Rₜ_biomass
true
```

# References

  - [Arnoldi et al. 2018 - How ecosystems recover from pulse perturbations]
    (https://doi.org/10.1016/j.jtbi.2017.10.003)

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

See also [`resilience`](@ref), [`median_reactivity`](@ref).
"""
function median_return_rate(
    jacobian::AbstractArray;
    perturbation::Symbol = :uniform,
    B_eq = nothing,
    threshold = 1e-3,
    δt = 1,
    t_safe = 1_000,
)
    if perturbation ∉ [:uniform, :prop_to_biomass]
        throw(
            ArgumentError(
                "`perturbation` should be either `:uniform` or `:prop_to_biomass`.",
            ),
        )
    end
    if perturbation == :prop_to_biomass && isnothing(B_eq)
        throw(
            ArgumentError(
                "If selecting `perturbation = :prop_to_biomass`, " *
                "equilibrium biomasses (`B_eq`) should be given.",
            ),
        )
    end
    n = size(jacobian, 1)
    C = perturbation == :uniform ? (1 / n) * I(n) : Diagonal(B_eq)^2 / sum(B_eq .^ 2)
    t = 1
    Rₜ, Rₜ_previous = median_return_rate(jacobian, C, t), 0
    while (abs(Rₜ - Rₜ_previous) > threshold * δt) && (t < t_safe)
        t += δt
        Rₜ_previous = Rₜ
        Rₜ = median_return_rate(jacobian, C, t)
    end
    !isinf(Rₜ) || @error "Return rate is Inf: please check the `jacobian` or decrease `δt`."
    t < t_safe || @error "Return rate did not converge: please check the `jacobian`."
    Rₜ
end
function median_return_rate(jacobian, C, t)
    -log(tr(exp(jacobian * t) * C * exp(transpose(jacobian) * t)) - log(tr(C))) / (2 * t)
end

# Reactivity
# ----------
"""
    reactivity(jacobian::AbstractArray)

Maximimal instantaneous rate at which an initial perturbation can be amplified.

```math
R_0 = - \\frac{\\max_i \\lambda_i (J + J^\\text{T})}{2}
```

# Examples

```jldoctest
julia> jacobian = [-1 0; 0 -2];

julia> reactivity(jacobian)
1.0
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Neubert and Caswell 1997 - Alternatives to resilience for ecological systems]
    (https://doi.org/10.1890/0012-9658(1997)078[0653:ATRFMT]2.0.CO;2)
  - [Arnoldi et al. 2016 - Resilience, reactivity and variability]
    (https://doi.org/10.1016/j.jtbi.2015.10.012)

See also [`resilience`](@ref).
"""
function reactivity(jacobian::AbstractArray)
    -(maximum(eigvals(jacobian + transpose(jacobian)))) / 2
end

"""
    median_reactivity(
        jacobian::AbstractArray;
        perturbation::Symbol = :uniform,
        B_eq = nothing,
    )

Median of the initial return rate to equilibrium.

If the perturbation is assumed to be the same for all species on average
(case where `perturbation = :uniform`), then:

```math
\\mathbb{M}(R_0) = - \\frac{1}{n} \\text{Tr} J
```

where ``J`` is the Jacobian matrix of size ``(n,n)``.

If the perturbation is assumed to be proportional to the species biomasses at equilibrium
given by the keyword argument `B_eq`
(case where `perturbation = :prop_to_biomass`), then:

```math
\\mathbb{M}(R_0) = - \\frac{\\text{Tr}(CJ)}{\\text{Tr}C}
```

where ``C`` is the correlation matrix of perturbations.
Specifically, for this perturbation
``C = \\frac{1}{\\sum_i B_{\\text{eq},ii}^2} \\text{diag}(B_\\text{eq})^2``.

By definition, if the Jacobian is stable ``\\mathbb{M}(R_0)`` is always positive
and larger than the asymptotic return rate ``R_\\infty``.

# Keyword arguments

  - `perturbation` gives the type of perturbation:
    either `:uniform` if species are assumed to be perturbed equally,
    or `:prop_to_biomass` if species are perturbed proportionnally to their biomasses at
    equilibrium. By default `perturbation = :uniform`.

  - `B_eq` species biomasses at equilibrium.
    Should only be provided if `perturbation = :prop_to_biomass`.
    By default `B_eq = nothing`.

# Examples

```jldoctest
julia> J = [-1 0; 0 -2];

julia> reactivity(J)
1.0

julia> median_reactivity(J)
1.5

julia> B_eq = [0.5, 1];

julia> median_reactivity(J; perturbation = :prop_to_biomass, B_eq = B_eq)
1.8
```

# References

  - [Arnoldi et al. 2018 - How ecosystems recover from pulse perturbations]
    (https://doi.org/10.1016/j.jtbi.2017.10.003)

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

See also [`reactivity`](@ref), [`median_return_rate`](@ref), [`resilience`](@ref).
"""
function median_reactivity(
    jacobian::AbstractArray;
    perturbation::Symbol = :uniform,
    B_eq = nothing,
)
    perturbation == :uniform && return -tr(jacobian) / size(jacobian, 1)
    if perturbation != :prop_to_biomass
        throw(
            ArgumentError(
                "`perturbation` should be either `:uniform` or `:prop_to_biomass`.",
            ),
        )
    else
        !isnothing(B_eq) || throw(
            ArgumentError(
                "If selecting `perturbation = :prop_to_biomass`, " *
                "equilibrium biomasses (`B_eq`) should be given.",
            ),
        )
        length(B_eq) == size(jacobian, 1) || throw(
            ArgumentError(
                "Incompatible sizes: `B_eq` should be of length S " *
                "and `jacobian` of size (S, S), where S is the species richness.",
            ),
        )
        C = Diagonal(B_eq)^2 / sum(B_eq .^ 2)
    end
    -tr(C * jacobian) / tr(C)
end

# Maximum amplification
# ---------------------
"""
    maximum_amplification(jacobian::AbstractArray; δt = 0.1, t_safe = 1_000)

Given a jacobian (``J``), returns the maximum amplification (``A_\\text{max}``)
and the corresponding time when the maximum amplification is reached (``t_\\text{max}``).

``A_\\text{max}`` is the largest factor by which the perturbation can be amplified.
It is defined as the maximum value of the amplification envelope ``A(t)`` where:

```math
A(t) = |||e^{Jt}||| = \\text{max}_x \\frac{||e^{Jt}x||}{||x||}
```

!!! note


The triple-norm ``|||.|||`` corresponds to spectral norm of the matrix
or its largest singular value
and the matrix norm ``||.||`` is the Forbenius norm.

Then ``A_\\text{max}=\\max_t A(t)`` and ``t_\\text{max}=\\{t~|~A(t) = A_\\text{max}\\}``.

If the system is not reactive i.e. ``R_0(J) \\geq 0``
we assume that ``A_\\text{max} = 1`` and ``t_\\text{max} = 0``.

# Keyword arguments

  - `δt` controls the size of the timestep at which the ``A(t)`` are computed,
    by default `δt = 0.01`.

  - `t_safe` is the maximum time after which the `while` loop is stopped for safety reasons,
    by default `t_safe = 1_000`.

# Examples

```jldoctest
julia> A = [-1 1; 0 -1];

julia> reactivity(A) # system is not reactive
0.5

julia> maximum_amplification(A)
(A_max = 1.0, t_max = 0.0)

julia> B = [-1 5; 0 -2];

julia> reactivity(B) < 0 # reactive system
true

julia> maximum_amplification(B).A_max ≈ 1.3836012
true

julia> maximum_amplification(B).t_max ≈ 0.56
true
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Neubert and Caswell 1997 - Alternatives to resilience for ecological systems]
    (https://doi.org/10.1890/0012-9658(1997)078[0653:ATRFMT]2.0.CO;2)

See also [`median_maximum_amplification`](@ref).
"""
function maximum_amplification(jacobian; δt = 0.01, t_safe = 1_000)
    reactivity(jacobian) <= 0 || return (A_max = 1.0, t_max = 0.0)
    t = 0
    A_previous, A = 0.0, 1.0
    while (A > A_previous && t < t_safe)
        t += δt
        A_previous = A
        A = opnorm(exp(t * jacobian), 2)
    end
    (A_max = A_previous, t_max = t - δt)
end

"""
    median_maximum_amplification(
        jacobian::AbstractArray;
        perturbation = :uniform,
        threshold = 1e-3,
        δt = 0.01,
        t_safe = 1_000,
        )

Return the median over the perturbation directions
of the maximum factor by which a perturbation can be amplified
and the time when this maximum amplification is reached.

The median amplification at time ``t`` is given by:

```math
\\mathbb{M}(A(t)) \\approx \\sqrt{\\text{Tr}(C e^{J^\\text{T}t} e^{Jt})}
```

where ``J`` is the Jacobian and ``C`` correlation perturbation matrix.
Specifically, if the perturbation is assumed to be equal on average for all species
(case where `perturbation = :uniform`)
then ``C = \\frac{1}{n} \\mathbb{1}``.
If the perturbation is assumed to be proportional to the equilibrium species biomasses
given by `B_eq` (case where `perturbation = :prop_to_biomass`).
then ``C = \\frac{1}{\\sum_i B_{\\text{eq},ii}^2} \\text{diag}(B_\\text{eq})^2``.

The function returns the tuple of:

  - `A_max`: ``\\mathbb{M}(A_\\text{max}) = \\max_t \\mathbb{M}(A(t))``

  - `t_max`: ``t_\\text{max} = \\{t~|~\\mathbb{M}(A(t)) = \\mathbb{M}(A_\\text{max}))``

If there is no amplification ``(\\mathbb{M}(A_\\text{max}) = 1,~t_\\text{max} = 0)``.

# Keyword arguments

  - `perturbation` gives the type of perturbation:
    either `:uniform` if species are assumed to be perturbed equally,
    or `:prop_to_biomass` if species are perturbed proportionnally to their biomasses at
    equilibrium. By default `perturbation = :uniform`.

  - `B_eq` species biomasses at equilibrium.
    Should only be provided if `perturbation = :prop_to_biomass`.
    By default `B_eq = nothing`.
  - `threshold` gives the minimal amplification ``A(t)`` after which we stop to look at,
    if we call ``\\tau`` the time at which the threshold is reached
    then we consider that the maximum of the amplification is reached in ``[0, \\tau]``.
    By default `threshold = 1e-3`.
  - `δt` controls the size of the timestep at which the ``A(t)`` are computed,
    by default `δt = 0.01`.
  - `t_safe` is the maximum time after which the `while` loop is stopped for safety reasons,
    by default `t_safe = 1_000`.

# Examples

```jldoctest
julia> jacobian = [-1 0; 0 -2]; # not reactive

julia> median_maximum_amplification(jacobian)
(A_max = 1.0, t_max = 0.0)

julia> jacobian = [-1 5; 0 -0.5]; # highly reactive

julia> ampli = median_maximum_amplification(jacobian);

julia> ampli.A_max > 1.0 && ampli.t_max > 0.0
true

julia> p, B_eq = :prop_to_biomass, [1, 2];

julia> ampli = median_maximum_amplification(jacobian; perturbation = p, B_eq = B_eq);

julia> ampli.A_max > 1.0 && ampli.t_max > 0.0
true
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Arnoldi et al. 2018 - How ecosystems recover from pulse perturbations]
    (https://doi.org/10.1016/j.jtbi.2017.10.003)

See also [`maximum_amplification`](@ref).
"""
function median_maximum_amplification(
    jacobian::AbstractArray;
    perturbation = :uniform,
    B_eq = nothing,
    threshold = 1e-3,
    δt = 0.01,
    t_safe = 1_000,
)
    if perturbation ∉ [:uniform, :prop_to_biomass]
        throw(
            ArgumentError(
                "`perturbation` should be either `:uniform` or `:prop_to_biomass`.",
            ),
        )
    end
    if perturbation == :prop_to_biomass && isnothing(B_eq)
        throw(
            ArgumentError(
                "If selecting `perturbation = :prop_to_biomass`, " *
                "equilibrium biomasses (`B_eq`) should be given.",
            ),
        )
    end
    reactivity(jacobian) < 0 || return (A_max = 1.0, t_max = 0.0)
    n = size(jacobian, 1)
    C = perturbation == :uniform ? (1 / n) * I(n) : Diagonal(B_eq)^2 / sum(B_eq .^ 2)
    max_amplification, amplification = 1.0, 1.0
    t_max, t = 0.0, 0.0
    while amplification > threshold && t < t_safe
        t += δt
        amplification = median_amplification(jacobian, C, t)
        if amplification > max_amplification
            max_amplification = amplification
            t_max = t
        end
    end
    t < t_safe || @error "Amplification did not converge: please check the `jacobian`."
    (A_max = max_amplification, t_max = t_max)
end
function median_amplification(jacobian, C, t)
    sqrt(tr(C * exp(transpose(jacobian) * t) * exp(jacobian * t)))
end

# Stochatic invariability
# -----------------------
"""
    stochastic_invariability(jacobian::AbstractArray)

Response of the linearized system to stochastic pertubations (white noise).
This response is given by:

```math
I_s = \\frac{1}{2 |||\\hat{J}^{-1}|||}
```

where ``\\hat{J}`` is the operator associated to the Jacobian ``J``
and ``|||.|||`` is the spectral norm.
Moreover, the operator norm can be computed as follow:
``|||\\hat{J}^{-1}||| = |||(J \\otimes 1 + 1 \\otimes J)^{-1}|||``.

# Examples

```jldoctest
julia> using LinearAlgebra;

julia> stochastic_invariability(I(5)) # |||I||| = 1
0.5
```

# References

  - [Dominguez-Garcia et al. 2019 - Unveilling dimension of stability]
    (https://doi.org/10.1073/pnas.190447011)

  - [Arnoldi et al. 2016 - Resilience, reactivity and variability]
    (https://doi.org/10.1016/j.jtbi.2015.10.012)
"""
function stochastic_invariability(jacobian::AbstractArray)
    n = size(jacobian, 1)
    opnorm(inv(kron(jacobian, I(n)) + kron(I(n), jacobian)))
end
